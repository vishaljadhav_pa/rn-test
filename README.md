# ASSIGNMENT #

You need to build employee onboarding app for the small organisation; this application will allow employees to register details and browse employee directory.

### First Screen ###

It will have a registration form with following fields —

* Name
* Age
* D.O.J
* Department
* Locality
* Skillset

With a submit button you can simply mock a submit API call with services like Beeceptor. Also ensure you show an alert to the user if they click back while editing the form.

### Second Screen ###

Second screen in the tab (Users/Reports) should have a searchable list of all the employees (JSON data can be mocked using a tool like Mockaroo). 
The list can have the name, department and skillset of the employee. The entire list should be searchable with a single text input by the employee name. Clicking on emaployee in the list will open a new screen that contains all the details collected in the first form.

### Third Screen ###

The third screen will have useful reports regarding the employees. The reports you'll need to build are —

* Number of people in a given skillset.
* Number of people by department.


You can use either React Native or Expo to build the app. Should have steps for generating the app for both iOS and Android.



### Additional Notes ###
* Please submit the solution via Github. If you're not a Github user or if you're using a private repo, bundle your Git repo and include it as an email attachment.
* Use of the redux is important
* Bonus points for unit tests and coverage.
* Bonus points for good commit messages.
* Bonus points if graphs are used for the reports.
* Bonus points for "better UX" — however you interpret & showcase it.